package sistema.bancodedados;

import java.util.Collection;
import java.util.HashMap;

import sistema.atas.AtaDeReuniao;

public class BancoDeDados {
	
	private HashMap <String, AtaDeReuniao> atas;
	
	public BancoDeDados (Collection<AtaDeReuniao> atasDeReuniao) {
		this();
		
		for (AtaDeReuniao ata : atasDeReuniao) {
			this.inserirAta(ata);
		}
	}	
	
	public BancoDeDados () {
		atas = new HashMap <> ();
	}	
	
	public HashMap<String, AtaDeReuniao> getAtas() {
		return atas;
	}
	
	public void inserirAta (AtaDeReuniao ata) {
		this.atas.put(ata.getTitulo(), ata);
	}
	
	public void removeAta (String tituloDaAta) {
		atas.remove(tituloDaAta);
	}
	
	public AtaDeReuniao consultaAta (String tituloDaAta) {
		return atas.get(tituloDaAta);
	}

	@Override
	public String toString() {
		return "<BancoDeDados> [ " + this.atas + " ]";
	}
	
}
