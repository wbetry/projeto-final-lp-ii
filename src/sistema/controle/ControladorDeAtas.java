package sistema.controle;

import sistema.atas.AtaDeReuniao;
import sistema.atas.Sugestao;
import sistema.bancodedados.BancoDeDados;
import sistema.empresa.Funcionario;
import sistema.empresa.FuncionarioInterno;
import sistema.exceptions.AtaException;
import sistema.exceptions.EstadoAtaException;
import sistema.exceptions.FuncionarioException;
import sistema.exceptions.SugestaoException;
import sistema.util.ArquivoBinario;
import sistema.util.ArquivoDeTexto;

public class ControladorDeAtas {

	private BancoDeDados atas;

	public ControladorDeAtas(BancoDeDados atas) {
		this.atas = atas;
	}

	public BancoDeDados getAtas() {
		return this.atas;
	}

	public void criarAta(AtaDeReuniao ata) {
		atas.inserirAta(ata);

		ArquivoBinario.escrever("storage/atasdereuniao", ata);
	}

	public AtaDeReuniao consultarAta(String tituloDaAta) throws AtaException {
		AtaDeReuniao ata = atas.consultaAta(tituloDaAta);
		
		if (ata == null) {
			throw new AtaException("Ata nao encontrada");
		}
		
		return ata;
	}

	public void realizarSugestao(String tituloDaAta, Sugestao sugestao) throws SugestaoException, EstadoAtaException {
		AtaDeReuniao ata = atas.consultaAta(tituloDaAta);

		ata.adicionarSugestao(sugestao);
	}

	public void alterarEstadoDaAtaParaEmRevisao(FuncionarioInterno funcionario, String tituloDaAta) throws EstadoAtaException, FuncionarioException {
		AtaDeReuniao ata = atas.consultaAta(tituloDaAta);

		String strEmail = ata.getTitulo() + " [";

		for (Funcionario funcionario1 : ata.getParticipantes()) {
			strEmail += funcionario1.getEmail() + ", ";
		}

		strEmail += "] \n";

		ArquivoDeTexto.escrever("storage/emails.txt", strEmail);

		ata.alteraEstadoParaEmRevisao(funcionario);
	}

	public void alterarEstadoDaAtaParaEmitida(FuncionarioInterno funcionario, String tituloDaAta, int visibilidade) throws EstadoAtaException, AtaException, FuncionarioException
			{
		AtaDeReuniao ata = atas.consultaAta(tituloDaAta);

		ata.alterarEstadoParaEmitida(ata.getEmissor(), visibilidade);
	}

}
