package sistema.controle;

import java.util.GregorianCalendar;
import java.util.HashSet;

import sistema.atas.AtaDeReuniao;
import sistema.atas.EstadoDaAta;
import sistema.bancodedados.BancoDeDados;
import sistema.empresa.FuncionarioInterno;
import sistema.empresa.Setor;
import sistema.relatorio.GeradorDeRelatorio;
import sistema.relatorio.TipoDeSaida;
import sistema.util.ArquivoDeTexto;
import sistema.util.DataFormatada;

public class ControladorDeRelatorios {

	private BancoDeDados atas;
	private int saida;

	public ControladorDeRelatorios(BancoDeDados atas, TipoDeSaida saida) {
		this.atas = atas;
		this.saida = saida.ordinal();
	}

	public void geraRelatorioTempo(GregorianCalendar tempoInicial, GregorianCalendar tempoFinal) {
		HashSet<AtaDeReuniao> atasNoPeriodo = atasNoPeriodo(tempoInicial, tempoFinal);

		StringBuilder relatorioTempo = new StringBuilder();

		relatorioTempo.append(String.format("Relatorio gerado para as atas entre os dias %s e %s\n",
				DataFormatada.formatar(tempoInicial), DataFormatada.formatar(tempoFinal)));

		relatorioTempo.append(String.format("Quantidade Total de Atas no periodo: %d\n",
				GeradorDeRelatorio.quantidadeTotalDeAtas(atasNoPeriodo)));

		relatorioTempo.append(String.format("Tempo medio das reunioes: %d\n",
				(GeradorDeRelatorio.tempoMedioDasReunioes(atasNoPeriodo)) / 60000));

		relatorioTempo.append(String.format("Reuniao com maior tempo: %d\n",
				(GeradorDeRelatorio.maiorTempoDasReunioes(atasNoPeriodo)) / 60000));

		relatorioTempo.append(String.format("Reuniao com menor tempo: %d\n",
				(GeradorDeRelatorio.menorTempoDasReunioes(atasNoPeriodo)) / 60000));

		this.gerarSaida(relatorioTempo.toString());
	}

	public void geraRelatorioSetor(GregorianCalendar tempoInicial, GregorianCalendar tempoFinal, Setor setor) {

		HashSet<AtaDeReuniao> atasNoPeriodo = atasNoPeriodo(tempoInicial, tempoFinal);

		StringBuilder relatorioSetor = new StringBuilder();

		relatorioSetor.append(String.format("Relatorio gerado para as atas entre os dias %s e %s\n",
				DataFormatada.formatar(tempoInicial), DataFormatada.formatar(tempoFinal)));

		relatorioSetor.append(String.format("Quantidade Total de Atas no periodo: %d\n",
				GeradorDeRelatorio.quantidadeTotalDeAtas(atasNoPeriodo)));

		relatorioSetor.append(String.format("Atas criadas pelo setor: %d\n",
				GeradorDeRelatorio.quantidadeAtasSetor(atasNoPeriodo, setor)));

		relatorioSetor.append(String.format("Percentual de atas criadas pelo sertor: %02.2f%%\n",
				GeradorDeRelatorio.percentualAtasSetor(atasNoPeriodo, setor)));

		this.gerarSaida(relatorioSetor.toString());
	}

	public void geraRelatorioEmissor(GregorianCalendar tempoInicial, GregorianCalendar tempoFinal,
			FuncionarioInterno funcionarioEmissor) {

		HashSet<AtaDeReuniao> atasNoPeriodo = atasNoPeriodo(tempoInicial, tempoFinal);

		StringBuilder relatorioEmissor = new StringBuilder();

		relatorioEmissor.append(String.format("Relatorio gerado para as atas entre os dias %s e %s\n",
				DataFormatada.formatar(tempoInicial), DataFormatada.formatar(tempoFinal)));

		relatorioEmissor.append(String.format("Quantidade Total de Atas no periodo: %d\n",
				GeradorDeRelatorio.quantidadeTotalDeAtas(atasNoPeriodo)));

		relatorioEmissor.append(String.format("Atas criadas pelo funcionario: %d\n",
				GeradorDeRelatorio.quantidadeAtasCriadasPorFuncionario(atasNoPeriodo, funcionarioEmissor)));

		relatorioEmissor.append(String.format("Percentual de atas criadas pelo funcionario: %02.2f%%\n",
				GeradorDeRelatorio.percentualAtasCriadasPorFuncionario(atasNoPeriodo, funcionarioEmissor)));

		this.gerarSaida(relatorioEmissor.toString());
	}

	private HashSet<AtaDeReuniao> atasNoPeriodo(GregorianCalendar tempoInicial, GregorianCalendar tempoFinal) {
		HashSet<AtaDeReuniao> atasNoPeriodo = new HashSet<>();

		for (String tituloDaAta : this.atas.getAtas().keySet()) {
			AtaDeReuniao ataDeReuniao = this.atas.getAtas().get(tituloDaAta);

			if (ataDeReuniao.getEstado() != EstadoDaAta.EMITIDA.ordinal()) {
				continue;
			}
			
			if (ataDeReuniao.getDataDeEmissao().after(tempoInicial)
					&& ataDeReuniao.getDataDeEmissao().before(tempoFinal)) {
				atasNoPeriodo.add(ataDeReuniao);
			}
		}

		return atasNoPeriodo;
	}

	private void gerarSaida(String relatorio) {
		if (this.saida == TipoDeSaida.TELA.ordinal()) {
			System.out.println(relatorio  + "\n");
		}

		if (this.saida == TipoDeSaida.ARQUIVO_DE_TEXTO.ordinal()) {
			ArquivoDeTexto.escrever("storage/relatorio.txt", relatorio  + "\n");
		}
	}

}