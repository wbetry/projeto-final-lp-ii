package sistema.exceptions;

public class EstadoAtaException extends Exception {

	private static final long serialVersionUID = -318788764174050373L;
	private String message;

	public EstadoAtaException() {

	}

	public EstadoAtaException(String mensagem) {
		this.message = mensagem;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

}
