package sistema.exceptions;

public class FuncionarioException extends Exception {

	private static final long serialVersionUID = -8258215392637538029L;
	private String message;

	public FuncionarioException() {

	}

	public FuncionarioException(String mensagem) {
		this.message = mensagem;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

}
