package sistema.exceptions;

public class ParticipantesException extends Exception {

	private static final long serialVersionUID = 7990395759218285612L;
	private String message;

	public ParticipantesException() {

	}

	public ParticipantesException(String mensagem) {
		this.message = mensagem;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

}
