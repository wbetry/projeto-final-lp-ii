package sistema.exceptions;

public class AtaException extends Exception {

	private static final long serialVersionUID = -8258215392637538029L;
	private String message;

	public AtaException() {

	}

	public AtaException(String mensagem) {
		this.message = mensagem;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

}
