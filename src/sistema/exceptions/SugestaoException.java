package sistema.exceptions;

public class SugestaoException extends Exception {

	private static final long serialVersionUID = 4305024174376120548L;
	private String message;

	public SugestaoException() {

	}

	public SugestaoException(String mensagem) {
		this.message = mensagem;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

}
