package sistema.empresa;

import java.io.Serializable;

public class FuncionarioExterno extends Funcionario implements Serializable {

	private static final long serialVersionUID = 7118605177895695826L;
	private String empresa;

	public FuncionarioExterno(String nome, String email) {
		super(nome, email);
	}

	public FuncionarioExterno(String nome, String email, String empresa) {
		this(nome, email);
		this.empresa = empresa;
	}

	public String getEmpresa() {
		return this.empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	@Override
	public String toString() {
		StringBuilder ata = new StringBuilder();

		ata.append(String.format("<FuncionarioExterno> "));
		ata.append("[ ");
		ata.append(String.format("Nome: %s, ", this.getNome()));
		ata.append(String.format("Email: %s, ", this.getEmail()));
		ata.append(String.format("Empresa: %s ", this.empresa));
		ata.append("]");

		return ata.toString();
	}

}
