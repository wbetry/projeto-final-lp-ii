package sistema.empresa;

import java.io.Serializable;

public class FuncionarioInterno extends Funcionario implements Serializable {

	private static final long serialVersionUID = -8890351306072532423L;
	private Setor setor;

	public FuncionarioInterno(String nome, String email) {
		super(nome, email);
	}
	
	public FuncionarioInterno(String nome, String email, Setor setor) {
		this(nome, email);
		this.setor = setor;
	}

	public Setor getSetor() {
		return this.setor;
	}

	@Override
	public String toString() {
		StringBuilder ata = new StringBuilder();

		ata.append(String.format("(FuncionarioInterno) "));
		ata.append("[ ");
		ata.append(String.format("Nome: %s, ", this.getNome()));
		ata.append(String.format("Email: %s ", this.getEmail()));
		ata.append("]");

		return ata.toString();
	}

}
