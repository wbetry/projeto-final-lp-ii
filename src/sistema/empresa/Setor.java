package sistema.empresa;

import java.io.Serializable;

public class Setor implements Serializable {

	private static final long serialVersionUID = 8053562680106376317L;
	private String nome;

	public Setor(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return this.nome;
	}
}
