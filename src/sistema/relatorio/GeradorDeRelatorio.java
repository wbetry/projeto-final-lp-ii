package sistema.relatorio;

import java.util.Collection;
import java.util.GregorianCalendar;

import sistema.atas.AtaDeReuniao;
import sistema.empresa.FuncionarioInterno;
import sistema.empresa.Setor;

public class GeradorDeRelatorio {

	public static double percentualAtasSetor(Collection<AtaDeReuniao> atas, Setor setor) {
		int quantidadeTotalAtas = atas.size();
		int quantidadeTotalAtasNoSetor = quantidadeAtasSetor(atas, setor);

		return ((double) quantidadeTotalAtasNoSetor / quantidadeTotalAtas) * 100;
	}

	public static int quantidadeAtasSetor(Collection<AtaDeReuniao> atas, Setor setor) {
		int quantidadeTotalAtasNoSetor = 0;

		for (AtaDeReuniao ataDeReuniao : atas) {
			if (ataDeReuniao.getSetor().equals(setor)) {
				quantidadeTotalAtasNoSetor += 1;
			}
		}

		return quantidadeTotalAtasNoSetor;
	}

	public static double percentualAtasCriadasPorFuncionario(Collection<AtaDeReuniao> atas, FuncionarioInterno funcionarioEmissor) {
		int quantidadeTotalAtas = atas.size();
		int quantidadeTotalAtasFuncionario = quantidadeAtasCriadasPorFuncionario(atas, funcionarioEmissor);
		
		return ((double) quantidadeTotalAtasFuncionario / quantidadeTotalAtas) * 100;
	}

	public static int quantidadeAtasCriadasPorFuncionario(Collection<AtaDeReuniao> atas, FuncionarioInterno funcionarioEmissor) {
		int quantidadeTotalAtasFuncionario = 0;

		for (AtaDeReuniao ataDeReuniao : atas) {
			if (ataDeReuniao.getEmissor().equals(funcionarioEmissor)) {
				quantidadeTotalAtasFuncionario += 1;
			}
		}

		return quantidadeTotalAtasFuncionario;
	}

	public static long tempoMedioDasReunioes(Collection<AtaDeReuniao> atas) {
		long tempoTotal = 0;

		for (AtaDeReuniao ataDeReuniao : atas) {
			GregorianCalendar inicio = ataDeReuniao.getHoraDeInicio();
			GregorianCalendar fim = ataDeReuniao.getHoraDeTermino();

			long tempoReuniao = fim.getTimeInMillis() - inicio.getTimeInMillis();

			tempoTotal += tempoReuniao;
		}

		return tempoTotal / atas.size();
	}

	public static long menorTempoDasReunioes(Collection<AtaDeReuniao> atas) {
		long menorTempo = tempoMedioDasReunioes(atas);

		for (AtaDeReuniao ataDeReuniao : atas) {
			GregorianCalendar inicio = ataDeReuniao.getHoraDeInicio();
			GregorianCalendar fim = ataDeReuniao.getHoraDeTermino();

			long tempoReuniao = fim.getTimeInMillis() - inicio.getTimeInMillis();

			if (tempoReuniao < menorTempo) {
				menorTempo = tempoReuniao;
			}
		}
		
		return menorTempo;
	}

	public static long maiorTempoDasReunioes(Collection<AtaDeReuniao> atas) {
		long maiorTempo = tempoMedioDasReunioes(atas);

		for (AtaDeReuniao ataDeReuniao : atas) {
			GregorianCalendar inicio = ataDeReuniao.getHoraDeInicio();
			GregorianCalendar fim = ataDeReuniao.getHoraDeTermino();

			long tempoReuniao = fim.getTimeInMillis() - inicio.getTimeInMillis();

			if (tempoReuniao > maiorTempo) {
				maiorTempo = tempoReuniao;
			}
		}

		return maiorTempo;
	}
	
	public static int quantidadeTotalDeAtas(Collection<AtaDeReuniao> atas) {
		return atas.size();
	}

}