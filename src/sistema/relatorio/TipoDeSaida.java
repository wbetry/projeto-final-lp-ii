package sistema.relatorio;

public enum TipoDeSaida {

	TELA, ARQUIVO_DE_TEXTO, ARQUIVO_BINARIO;

}
