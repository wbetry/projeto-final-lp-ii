package sistema.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class ArquivoDeTexto {

	public static void escrever(String nomeDoArquivo, Object texto) {
		try {
			FileOutputStream arquivo = new FileOutputStream(nomeDoArquivo, true);

			PrintWriter escrita = new PrintWriter(arquivo);

			escrita.print(texto);
			
			escrita.close();
			arquivo.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*public static HashSet<Object> ler(String nomeDoArquivo, AbstractLeitor leitor, String token) {
		HashSet<Object> conjuntoDeDados = new HashSet<>();
		
		try {
			Scanner arquivo = new Scanner(new File(nomeDoArquivo));
			
			while (arquivo.hasNextLine()) {
				String leitura = arquivo.nextLine();
				
				StringTokenizer tokenizer = new StringTokenizer(leitura, token);
				
				conjuntoDeDados.add(leitor.ler(tokenizer));				
			}
			
			arquivo.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return conjuntoDeDados;
	}
*/
}
	

