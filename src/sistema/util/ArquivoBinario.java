package sistema.util;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;

import sistema.atas.AtaDeReuniao;

public class ArquivoBinario {

	public static void escrever(String nomeDoArquivo, AtaDeReuniao ata) {
		try {
			FileOutputStream arquivo = new FileOutputStream(nomeDoArquivo, true);

			ObjectOutputStream escrita = new ObjectOutputStream(arquivo);

			escrita.writeObject(ata);

			escrita.close();
			arquivo.close();
		} catch (FileNotFoundException e) {
			System.out.println("Impossível realizar a operação! Arquivo nao existe");
		} catch (IOException e) {
			//System.out.println("Erro ao editar arquivo \"" + nomeDoArquivo + "\"");
			e.printStackTrace();
		}
	}

	public static HashSet<AtaDeReuniao> ler(String nomeDoArquivo) {
		HashSet<AtaDeReuniao> atas = new HashSet<>();

		try {
			FileInputStream arquivo = new FileInputStream(nomeDoArquivo);

			ObjectInputStream streamEntrada = new ObjectInputStream(arquivo);

			try {
				while (true) {
					AtaDeReuniao objeto = (AtaDeReuniao) streamEntrada.readObject();
					atas.add((AtaDeReuniao) objeto);
				}
			} catch (EOFException e) {
				System.out.println("Fim do Arquivo");
			} catch (ClassNotFoundException e) {
				System.out.println(e.getMessage());
			} finally {
				streamEntrada.close();
			}
		} catch (FileNotFoundException e) {
			System.out.println("Impossível realizar a operação! Arquivo nao existe");
		} catch (IOException e) {
			System.out.println("Erro ao ler arquivo \"" + nomeDoArquivo + "\"");
		}

		return atas;
	}

}