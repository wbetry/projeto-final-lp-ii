package sistema.util;

import java.util.GregorianCalendar;

public class DataFormatada {

	public static String formatar(GregorianCalendar data) {
		StringBuilder dataStr = new StringBuilder();

		dataStr.append(String.format("%02d/%02d/%04d", data.get(GregorianCalendar.DAY_OF_MONTH),
				data.get(GregorianCalendar.MONTH) + 1, data.get(GregorianCalendar.YEAR)));
		dataStr.append(String.format(" %02d:%02d:%02d", data.get(GregorianCalendar.HOUR_OF_DAY),
				data.get(GregorianCalendar.MINUTE), data.get(GregorianCalendar.SECOND)));

		return dataStr.toString();
	}

}
