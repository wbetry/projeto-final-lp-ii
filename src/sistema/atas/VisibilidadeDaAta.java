package sistema.atas;

public enum VisibilidadeDaAta {

	PUBLICA, PROTEGIDA, PRIVADA;

}
