package sistema.atas;

import java.io.Serializable;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashSet;

import sistema.empresa.Funcionario;
import sistema.empresa.FuncionarioInterno;
import sistema.empresa.Setor;
import sistema.exceptions.AtaException;
import sistema.exceptions.EstadoAtaException;
import sistema.exceptions.FuncionarioException;
import sistema.exceptions.ParticipantesException;
import sistema.exceptions.SugestaoException;
import sistema.util.DataFormatada;

final public class AtaDeReuniao implements Serializable {

	private static final long serialVersionUID = -6624443265003848995L;
	private String titulo;
	private GregorianCalendar dataDeEmissao;
	private FuncionarioInterno emissor;
	private HashSet<Funcionario> participantes;
	private GregorianCalendar horaDeInicio;
	private GregorianCalendar horaDeTermino;
	private String pauta;
	private Setor setor;
	private String descricao;
	private HashSet<String> palavrasChave;
	private int visibilidade;
	private int status;
	private HashSet<Sugestao> sugestoes;
	private int saida;

	public AtaDeReuniao(String titulo, GregorianCalendar dataDeEmissao, FuncionarioInterno emissor,
			Collection<Funcionario> participantes, GregorianCalendar horaDeInicio,
			GregorianCalendar horaDeTermino, String pauta, Setor setor, String descricao,
			Collection<String> palavrasChave
	) throws ParticipantesException, EstadoAtaException, AtaException {
		this.titulo = titulo;
		this.dataDeEmissao = dataDeEmissao;

		this.participantes = new HashSet<>();

		this.emissor = emissor;
		this.participantes.add(emissor);

		for (Funcionario participante : participantes) {
			this.participantes.add(participante);
		}

		if (this.participantes.size() < 2) {
			throw new ParticipantesException("Para haver uma reuniao tem de estar presentes no minimo 2 participantes");
		}

		this.horaDeInicio = horaDeInicio;
		this.horaDeTermino = horaDeTermino;
		this.pauta = pauta;
		this.setor = setor;
		this.descricao = descricao;
		this.status = EstadoDaAta.CRIADA.ordinal();
		this.visibilidade = VisibilidadeDaAta.PRIVADA.ordinal();

		this.palavrasChave = new HashSet<>();

		for (String palavra : palavrasChave) {
			this.adicionarPalavraChave(palavra);
		}

		if (this.palavrasChave.size() < 1) {
			throw new AtaException("Para gerar uma ata tem de haver no minimo uma palavra chave");
		}

	}

	public String getTitulo() {
		return this.titulo;
	}

	public GregorianCalendar getDataDeEmissao() {
		return this.dataDeEmissao;
	}

	public FuncionarioInterno getEmissor() {
		return this.emissor;
	}

	public HashSet<Funcionario> getParticipantes() {
		return (HashSet<Funcionario>) this.participantes.clone();
	}

	public GregorianCalendar getHoraDeInicio() {
		return this.horaDeInicio;
	}

	public GregorianCalendar getHoraDeTermino() {
		return this.horaDeTermino;
	}

	public String getPauta() {
		return this.pauta;
	}

	public Setor getSetor() {
		return this.setor;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public HashSet<String> getPalavrasChave() {
		return this.palavrasChave;
	}

	public int getVisibilidade() {
		return this.visibilidade;
	}

	public int getEstado() {
		return this.status;
	}
	
	public int getSaida () {
		return this.saida;
	}
	
	public void adicionarSugestao(Sugestao sugestao) throws SugestaoException, EstadoAtaException {
		if (this.status != EstadoDaAta.EM_REVISAO.ordinal()) {
			throw new EstadoAtaException("A sugestao nao pode ser adicionada pois nao esta em estado de emitida");
		}

		if (this.sugestoes == null) {
			this.sugestoes = new HashSet<>();
		}
			
		this.sugestoes.add(sugestao);
	}

	public void alteraEstadoParaEmRevisao(FuncionarioInterno funcionario)
			throws EstadoAtaException, FuncionarioException {
		this.validaEmissor(funcionario);

		if (this.status != EstadoDaAta.CRIADA.ordinal()) {
			throw new EstadoAtaException("A ata nao pode ser ALTERADA para o estado de Revisao");
		}

		this.status = EstadoDaAta.EM_REVISAO.ordinal();

		this.visibilidade = VisibilidadeDaAta.PROTEGIDA.ordinal();
	}

	public void alterarEstadoParaEmitida(FuncionarioInterno funcionario, int visibilidade)
			throws EstadoAtaException, AtaException, FuncionarioException {
		this.validaEmissor(funcionario);

		if (this.status != EstadoDaAta.EM_REVISAO.ordinal()) {
			throw new EstadoAtaException("A Ata nao pode ser ALTERADA para o estado de Emitida");
		}

		if (visibilidade != VisibilidadeDaAta.PROTEGIDA.ordinal()
				&& visibilidade != VisibilidadeDaAta.PUBLICA.ordinal()) {
			throw new AtaException("Visibilidade invalida");
		}

		this.status = EstadoDaAta.EMITIDA.ordinal();

		this.visibilidade = visibilidade;
	}

	public void adicionarPalavraChave(String palavra) throws EstadoAtaException, AtaException {
		if (!(this.status == EstadoDaAta.CRIADA.ordinal() || this.status == EstadoDaAta.EM_REVISAO.ordinal())) {
			throw new EstadoAtaException("Nao pode adicionar palavras enquanto a ata nao esta modificavel!");
		}

		if (this.palavrasChave.size() == 5) {
			throw new AtaException("A ata ja possui mais de 5 palavras-chave");
		}

		palavrasChave.add(palavra);
	}

	@Override
	public String toString() {
		StringBuilder ata = new StringBuilder();

		ata.append(String.format("<AtaDeReuniao> "));
		ata.append("[");
		ata.append(String.format("\n\tTitulo: %s, ", this.titulo));
		ata.append(String.format("\n\tEmissor: %s, ", this.emissor));
		ata.append(String.format("\n\tHorario de Inicio: %s, ", DataFormatada.formatar(this.horaDeInicio)));
		ata.append(String.format("\n\tHorario de Termino: %s, ", DataFormatada.formatar(this.horaDeTermino)));
		ata.append(String.format("\n\tParticipantes: [%s], ", this.participantes));
		ata.append(String.format("\n\tPauta: %s", this.pauta));
		ata.append(String.format("\n\tSetor: %s", this.setor.getNome()));
		ata.append(String.format("\n\tVisibilidade: %d, ", this.visibilidade));
		ata.append(String.format("\n\tStatus: %d ", this.status));
		ata.append("\n]");

		return ata.toString();
	}

	private void validaEmissor(FuncionarioInterno funcionario) throws FuncionarioException {
		if (!this.emissor.equals(funcionario)) {
			throw new FuncionarioException("O funcionario digitado não é o emissor da ata");
		}
	}

}
