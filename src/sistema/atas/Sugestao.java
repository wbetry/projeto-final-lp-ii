package sistema.atas;

import java.util.GregorianCalendar;

public class Sugestao {

	private GregorianCalendar dataDeCriacao;
	private String descricao;

	public Sugestao(GregorianCalendar dataDeCriacao, String descricao) {
		this.dataDeCriacao = dataDeCriacao;
		this.descricao = descricao;
	}

	public GregorianCalendar getDataDeCriacao() {
		return dataDeCriacao;
	}

	public String getDescricao() {
		return descricao;
	}

}
