package sistema.atas;

import java.util.Comparator;

public class AtaComparator implements Comparator<AtaDeReuniao> {

	@Override
	public int compare(AtaDeReuniao ata1, AtaDeReuniao ata2) {
		return ata1.getTitulo().compareTo(ata2.getTitulo());
	}

}