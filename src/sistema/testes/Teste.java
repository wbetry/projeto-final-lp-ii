package sistema.testes;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import sistema.atas.AtaDeReuniao;
import sistema.bancodedados.BancoDeDados;
import sistema.controle.ControladorDeAtas;
import sistema.empresa.Funcionario;
import sistema.empresa.FuncionarioExterno;
import sistema.empresa.FuncionarioInterno;
import sistema.empresa.Setor;

public class Teste {

	public static void main(String[] args) {
		
		BancoDeDados bd = new BancoDeDados();
		
		ControladorDeAtas c = new ControladorDeAtas(bd);

		Setor setor = new Setor("Lorem");
		
		FuncionarioInterno emissor = new FuncionarioInterno("James", "james@example.com", setor);
		FuncionarioInterno f1 = new FuncionarioInterno("John", "john@example.com", setor);
		FuncionarioExterno f2 = new FuncionarioExterno("Joseh Papo", "papo@gmail.com");

		ArrayList<Funcionario> participantes = new ArrayList<>();
		participantes.add(f1);
		participantes.add(f2);
		participantes.add(emissor);

		
	
		ArrayList<String> palavrasChave = new ArrayList<>();
		palavrasChave.add("Lorem Ipsum");
		palavrasChave.add("Lorem Ipsum");

		try {
			AtaDeReuniao ata1 = new AtaDeReuniao("Lorem Ipsum", new GregorianCalendar(), emissor, participantes, new GregorianCalendar(),
					new GregorianCalendar(), "Lorem Ipsum", setor,
					"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\n"
							+ "tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\n"
							+ "quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\n"
							+ "consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\n"
							+ "cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\n"
							+ "proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
					palavrasChave);

			AtaDeReuniao ata2 = new AtaDeReuniao("Lorem Ipsum 2", new GregorianCalendar() ,emissor, participantes, new GregorianCalendar(),
					new GregorianCalendar(), "Lorem Ipsum", setor,
					"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\n"
							+ "tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\n"
							+ "quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\n"
							+ "consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\n"
							+ "cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\n"
							+ "proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
					palavrasChave);

			System.out.println(ata1);

			c.criarAta(ata1);
			c.criarAta(ata2);

			System.out.println(c.getAtas());

			c.alterarEstadoDaAtaParaEmRevisao(emissor, ata1.getTitulo());
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		
	}

}
