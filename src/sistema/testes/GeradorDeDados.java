package sistema.testes;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import sistema.atas.AtaDeReuniao;
import sistema.atas.Sugestao;
import sistema.bancodedados.BancoDeDados;
import sistema.empresa.Funcionario;
import sistema.empresa.FuncionarioExterno;
import sistema.empresa.FuncionarioInterno;
import sistema.empresa.Setor;
import sistema.exceptions.AtaException;
import sistema.exceptions.EstadoAtaException;
import sistema.exceptions.ParticipantesException;

public class GeradorDeDados {

	public static BancoDeDados gerarBancoDeDados(int quantidadeDeAtas, ArrayList<FuncionarioInterno> emissores) {
		BancoDeDados bd = new BancoDeDados();

		try {
			HashSet<AtaDeReuniao> atasDeReuniao = geraAtasDeReuniao(quantidadeDeAtas, emissores, 5);

			for (AtaDeReuniao ata : atasDeReuniao) {
				bd.inserirAta(ata);
			}
		} catch (ParticipantesException | EstadoAtaException | AtaException e) {
			e.printStackTrace();
		}

		return bd;
	}

	public static ArrayList<Setor> gerarSetores(int quantiadeDeSetores) {
		ArrayList<Setor> setores = new ArrayList<>();

		for (int i = 0; i < quantiadeDeSetores; i++) {
			Setor setor = new Setor("Setor " + i);

			setores.add(setor);
		}

		return setores;
	}

	public static ArrayList<FuncionarioInterno> gerarEmissores(int quantiadeDeEmissores, List<Setor> setores) {

		ArrayList<FuncionarioInterno> emissores = new ArrayList<>();

		int quantidadeDeSetores = setores.size();

		for (int i = 0; i < quantiadeDeEmissores; i++) {
			FuncionarioInterno funcionario = new FuncionarioInterno("Emissor " + i, "emissor" + i + "@example.com",
					setores.get((new Random().nextInt(quantidadeDeSetores))));

			emissores.add(funcionario);
		}

		return emissores;
	}

	public static ArrayList<Funcionario> gerarParticipantes(int quantidadeDeParticipantes) {

		ArrayList<Funcionario> funcionarios = new ArrayList<>();

		for (int i = 0; i < quantidadeDeParticipantes; i++) {

			String nome = "Funcionario " + i;
			String email = "funcionario" + i + "@example.com";

			Funcionario funcionario = ((new Random().nextInt(2)) == 0) ? new FuncionarioExterno(nome, email)
					: new FuncionarioInterno(nome, email);

			funcionarios.add(funcionario);
		}

		return funcionarios;
	}

	public static ArrayList<String> gerarPalavrasChave(int quantidadeDePalavraschave) {
		ArrayList<String> palavrasChave = new ArrayList<>();

		for (int i = 0; i < quantidadeDePalavraschave; i++) {
			palavrasChave.add("Palavra" + (new Random().nextInt(100)));
		}

		return palavrasChave;
	}

	public static HashSet<AtaDeReuniao> geraAtasDeReuniao(int numero, List<FuncionarioInterno> emissores,
			int maximoDeParticipantes) throws ParticipantesException, EstadoAtaException, AtaException {

		HashSet<AtaDeReuniao> atas = new HashSet<>();

		int tamanhoColecaoEmissores = emissores.size();

		for (int i = 0; i < numero; i++) {
			int indiceDoEmissor = (new Random().nextInt(tamanhoColecaoEmissores));
			int quantidadeTotalDeParticipantes = (new Random().nextInt(maximoDeParticipantes - 1)) + 1;
			int quantidadeTotalDePalavrasChave = (new Random().nextInt(4)) + 1;

			ArrayList<Funcionario> participantes = gerarParticipantes(quantidadeTotalDeParticipantes);

			ArrayList<String> palavrasChaveGeradas = gerarPalavrasChave(quantidadeTotalDePalavrasChave);

			FuncionarioInterno emissorAdaAta = emissores.get(indiceDoEmissor);

			try {
				int dia = (new Random().nextInt(27) + 1);
				int mes = (new Random().nextInt(11) + 1);

				int hora = (new Random().nextInt(23));

				int minutoTermino = (new Random().nextInt(59) + 1);

				AtaDeReuniao ata = new AtaDeReuniao("Lorem Ipsum " + i,
						new GregorianCalendar(2017, mes, dia + 1, hora, 0), emissorAdaAta, participantes,
						new GregorianCalendar(2017, mes, dia, hora, 0),
						new GregorianCalendar(2017, mes, dia, hora, minutoTermino), "Lorem Ipsum " + i,
						emissorAdaAta.getSetor(), "Lorem ipsum " + i, palavrasChaveGeradas);

				atas.add(ata);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return atas;
	}

	public static ArrayList<Sugestao> gerarSugestoes(int quantidadeDeSugestoes, GregorianCalendar data) {
		ArrayList<Sugestao> sugestoes = new ArrayList<>();

		for (int i = 0; i < quantidadeDeSugestoes; i++) {
			Sugestao sugestao = new Sugestao(data, "Sugestao " + i);

			sugestoes.add(sugestao);
		}

		return sugestoes;
	}

}
