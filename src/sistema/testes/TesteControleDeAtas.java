package sistema.testes;

import java.util.ArrayList;
import java.util.Random;
import java.util.Set;

import sistema.atas.AtaDeReuniao;
import sistema.atas.Sugestao;
import sistema.atas.VisibilidadeDaAta;
import sistema.bancodedados.BancoDeDados;
import sistema.controle.ControladorDeAtas;
import sistema.empresa.FuncionarioInterno;
import sistema.empresa.Setor;
import sistema.exceptions.AtaException;
import sistema.exceptions.EstadoAtaException;
import sistema.exceptions.FuncionarioException;
import sistema.exceptions.SugestaoException;

public class TesteControleDeAtas {

	public static void main(String[] args) {

		ArrayList<Setor> setores = GeradorDeDados.gerarSetores(3);

		ArrayList<FuncionarioInterno> emissores = GeradorDeDados.gerarEmissores(5, setores);

		BancoDeDados bancoDeDados = GeradorDeDados.gerarBancoDeDados(100, emissores);

		ControladorDeAtas controle = new ControladorDeAtas(bancoDeDados);

		Set<String> titulosDasAtas = bancoDeDados.getAtas().keySet();

		for (String tituloDaAta : titulosDasAtas) {

			try {
				AtaDeReuniao ata = controle.consultarAta(tituloDaAta);

				try {
					controle.alterarEstadoDaAtaParaEmRevisao(ata.getEmissor(), tituloDaAta);
					ArrayList<Sugestao> sugestoes = GeradorDeDados.gerarSugestoes((new Random().nextInt(4)) + 1,
							ata.getDataDeEmissao());

					for (Sugestao sugestao : sugestoes) {
						try {
							ata.adicionarSugestao(sugestao);
						} catch (SugestaoException | EstadoAtaException e) {
							e.printStackTrace();
						}
					}
				} catch (EstadoAtaException | FuncionarioException e) {
					e.printStackTrace();
				}
			} catch (AtaException e) {
				e.printStackTrace();
			}

		}

		for (String tituloDaAta : titulosDasAtas) {

			try {
				AtaDeReuniao ata = controle.consultarAta(tituloDaAta);

				try {
					controle.alterarEstadoDaAtaParaEmitida(ata.getEmissor(), tituloDaAta,
							((new Random().nextInt()) % 2 == 0) ? VisibilidadeDaAta.PUBLICA.ordinal()
									: VisibilidadeDaAta.PROTEGIDA.ordinal());
				} catch (EstadoAtaException | AtaException | FuncionarioException e) {
					e.printStackTrace();
				}
			} catch (AtaException e1) {
				e1.printStackTrace();
			}
		}

		System.out.println(bancoDeDados);
	}

}
