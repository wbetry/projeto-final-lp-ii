package sistema.testes;

import sistema.atas.EstadoDaAta;
import sistema.atas.VisibilidadeDaAta;
import sistema.relatorio.TipoDeSaida;

public class TesteTiposEnumerados {

	public static void main(String[] args) {
		
		System.out.println(EstadoDaAta.CRIADA.ordinal());
		System.out.println(EstadoDaAta.EM_REVISAO.ordinal());
		System.out.println(EstadoDaAta.EMITIDA.ordinal());
		
		System.out.println(VisibilidadeDaAta.PUBLICA.ordinal());
		System.out.println(VisibilidadeDaAta.PROTEGIDA.ordinal());
		System.out.println(VisibilidadeDaAta.PRIVADA.ordinal());
		
		System.out.println(TipoDeSaida.TELA.ordinal());
		System.out.println(TipoDeSaida.ARQUIVO_DE_TEXTO.ordinal());
		System.out.println(TipoDeSaida.ARQUIVO_BINARIO.ordinal());
	}
	
}
