package sistema.testes;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.Set;

import sistema.atas.AtaDeReuniao;
import sistema.atas.VisibilidadeDaAta;
import sistema.bancodedados.BancoDeDados;
import sistema.controle.ControladorDeAtas;
import sistema.controle.ControladorDeRelatorios;
import sistema.empresa.FuncionarioInterno;
import sistema.empresa.Setor;
import sistema.exceptions.AtaException;
import sistema.exceptions.EstadoAtaException;
import sistema.exceptions.FuncionarioException;
import sistema.relatorio.TipoDeSaida;

public class TesteControleEstatisticas {

	public static void main(String[] args) {

		GregorianCalendar periodoInicio = new GregorianCalendar(2017, 5, 1);

		GregorianCalendar periodoTermino = new GregorianCalendar(2017, 11, 31);

		ArrayList<Setor> setores = GeradorDeDados.gerarSetores(3);

		ArrayList<FuncionarioInterno> emissores = GeradorDeDados.gerarEmissores(5, setores);

		BancoDeDados bancoDeDados = GeradorDeDados.gerarBancoDeDados(100, emissores);

		ControladorDeAtas controle = new ControladorDeAtas(bancoDeDados);

		Set<String> titulosDasAtas = bancoDeDados.getAtas().keySet();

		for (String tituloDaAta : titulosDasAtas) {
			try {
				AtaDeReuniao ata = controle.consultarAta(tituloDaAta);

				try {
					controle.alterarEstadoDaAtaParaEmRevisao(ata.getEmissor(), tituloDaAta);
				} catch (EstadoAtaException | FuncionarioException e) {
					e.printStackTrace();
				}
			} catch (AtaException e) {
				e.printStackTrace();
			}

		}

		for (String tituloDaAta : titulosDasAtas) {

			try {
				AtaDeReuniao ata = controle.consultarAta(tituloDaAta);

				try {
					controle.alterarEstadoDaAtaParaEmitida(ata.getEmissor(), tituloDaAta,
							((new Random().nextInt()) % 2 == 0) ? VisibilidadeDaAta.PUBLICA.ordinal()
									: VisibilidadeDaAta.PROTEGIDA.ordinal());
				} catch (EstadoAtaException | AtaException | FuncionarioException e) {
					e.printStackTrace();
				}
			} catch (AtaException e1) {
				e1.printStackTrace();
			}
		}

		ControladorDeRelatorios controleTela = new ControladorDeRelatorios(bancoDeDados, TipoDeSaida.TELA);

		controleTela.geraRelatorioEmissor(periodoInicio, periodoTermino, emissores.get(1));
		controleTela.geraRelatorioSetor(periodoInicio, periodoTermino, emissores.get(1).getSetor());
		controleTela.geraRelatorioTempo(periodoInicio, periodoTermino);

		ControladorDeRelatorios controleArquivoTexto = new ControladorDeRelatorios(bancoDeDados,
				TipoDeSaida.ARQUIVO_DE_TEXTO);

		controleArquivoTexto.geraRelatorioEmissor(periodoInicio, periodoTermino, emissores.get(2));
		controleArquivoTexto.geraRelatorioSetor(periodoInicio, periodoTermino, emissores.get(2).getSetor());
		controleArquivoTexto.geraRelatorioTempo(periodoInicio, periodoTermino);
	}
}
